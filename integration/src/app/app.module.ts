import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { OrderModule} from "angular4-order-pipe";

import { AppComponent }  from './app.component';

@NgModule({
  imports:      [ BrowserModule, OrderModule],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
