"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var angular4_order_pipe_1 = require("angular4-order-pipe");
var app_component_1 = require("./app.component");
var drop_area_component_1 = require("./drop-area/drop-area.component");
var navigation_component_1 = require("./navigation/navigation.component");
var generic_box_module_1 = require("./generic-box/generic-box.module");
var type_check_module_1 = require("./type-check/type-check.module");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            generic_box_module_1.GenericBoxModule,
            angular4_order_pipe_1.OrderModule,
            type_check_module_1.TypeCheckModule
        ],
        declarations: [
            app_component_1.AppComponent,
            drop_area_component_1.DropAreaComponent,
            navigation_component_1.NavigationComponent
        ],
        providers: [],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map