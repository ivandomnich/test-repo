"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var type_check_enum_1 = require("../type-check-enum");
var NavigationTypeCheckComponent = (function () {
    function NavigationTypeCheckComponent() {
        this.pegType = type_check_enum_1.TypeCheckEnum;
        this.itemsToDrop = [
            {
                name: 'Round Peg ',
                content: 'description 1',
                type: type_check_enum_1.TypeCheckEnum.Round
            },
            {
                name: 'Square Peg',
                content: 'description 2',
                type: type_check_enum_1.TypeCheckEnum.Square
            },
        ];
    }
    NavigationTypeCheckComponent.prototype.releaseDrop = function (event) {
        var _this = this;
        var index = this.itemsToDrop.indexOf(event);
        if (index >= 0) {
            setTimeout(function () { (_this.checkType(event, index), 100); });
        }
    };
    NavigationTypeCheckComponent.prototype.checkType = function (event, index) {
        if (event.type === this.dropItemType) {
            this.itemsToDrop.splice(index, 1);
        }
    };
    NavigationTypeCheckComponent.prototype.startDrag = function (item) {
        console.log('Begining to drag item: ' + item);
    };
    return NavigationTypeCheckComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], NavigationTypeCheckComponent.prototype, "dropItemType", void 0);
NavigationTypeCheckComponent = __decorate([
    core_1.Component({
        selector: 'app-navigation-type-check',
        templateUrl: './navigation-type-check.component.html',
        styleUrls: ['./navigation-type-check.component.css']
    }),
    __metadata("design:paramtypes", [])
], NavigationTypeCheckComponent);
exports.NavigationTypeCheckComponent = NavigationTypeCheckComponent;
//# sourceMappingURL=navigation-type-check.component.js.map