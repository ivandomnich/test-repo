import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GenericBoxModule } from '../generic-box/generic-box.module';
import { OrderModule } from "angular4-order-pipe";
import { TypeCheckComponent } from './type-check.component';
import { DropAreaTypeCheckComponent} from './drop-area-type-check/drop-area-type-check.component';
import { NavigationTypeCheckComponent } from './navigation-type-check/navigation-type-check.component';
import { TypeCheckService } from './type-check-service.service';

@NgModule({
  imports: [
    CommonModule,
    GenericBoxModule,
    OrderModule
  ],
  declarations: [
    DropAreaTypeCheckComponent,
    NavigationTypeCheckComponent,
    TypeCheckComponent
  ],
  exports: [TypeCheckComponent]
})
export class TypeCheckModule { }