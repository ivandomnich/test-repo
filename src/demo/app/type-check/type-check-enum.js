"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TypeCheckEnum;
(function (TypeCheckEnum) {
    TypeCheckEnum[TypeCheckEnum["Round"] = 0] = "Round";
    TypeCheckEnum[TypeCheckEnum["Square"] = 1] = "Square";
})(TypeCheckEnum = exports.TypeCheckEnum || (exports.TypeCheckEnum = {}));
//# sourceMappingURL=type-check-enum.js.map