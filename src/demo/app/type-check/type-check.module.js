"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var generic_box_module_1 = require("../generic-box/generic-box.module");
var angular4_order_pipe_1 = require("angular4-order-pipe");
var type_check_component_1 = require("./type-check.component");
var drop_area_type_check_component_1 = require("./drop-area-type-check/drop-area-type-check.component");
var navigation_type_check_component_1 = require("./navigation-type-check/navigation-type-check.component");
var TypeCheckModule = (function () {
    function TypeCheckModule() {
    }
    return TypeCheckModule;
}());
TypeCheckModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            generic_box_module_1.GenericBoxModule,
            angular4_order_pipe_1.OrderModule
        ],
        declarations: [
            drop_area_type_check_component_1.DropAreaTypeCheckComponent,
            navigation_type_check_component_1.NavigationTypeCheckComponent,
            type_check_component_1.TypeCheckComponent
        ],
        exports: [type_check_component_1.TypeCheckComponent]
    })
], TypeCheckModule);
exports.TypeCheckModule = TypeCheckModule;
//# sourceMappingURL=type-check.module.js.map