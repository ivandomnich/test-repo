"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var type_check_enum_1 = require("../type-check-enum");
var DropAreaTypeCheckComponent = (function () {
    function DropAreaTypeCheckComponent() {
        this.itemsDroppedRound = [];
        this.itemsDroppedSquare = [];
        this.warningMessage = '';
        this.highlight = ['', ''];
        this.typeCheck = type_check_enum_1.TypeCheckEnum;
        this.droppedItemType = new core_1.EventEmitter();
    }
    DropAreaTypeCheckComponent.prototype.addDropItem = function (event, type) {
        if (type === type_check_enum_1.TypeCheckEnum.Square) {
            if (event.type === type_check_enum_1.TypeCheckEnum.Square) {
                this.itemsDroppedSquare.push(event);
                this.droppedItemType.emit(event.type);
                this.warningMessage = '';
            }
            else {
                this.warningMessage = "Technically a round shape will fit in a square shap, try to match them";
            }
        }
        if (type === type_check_enum_1.TypeCheckEnum.Round) {
            if (event.type === type_check_enum_1.TypeCheckEnum.Round) {
                this.itemsDroppedRound.push(event);
                this.droppedItemType.emit(event.type);
                this.warningMessage = '';
            }
            else {
                this.warningMessage = "A square shape will not fit in a round shape.";
            }
        }
    };
    DropAreaTypeCheckComponent.prototype.dragEnter = function (event, type) {
        if (event.type !== type) {
            this.highlight[type] = 'badHighlight';
        }
        else {
            this.highlight[type] = 'highlight';
        }
    };
    DropAreaTypeCheckComponent.prototype.dragLeave = function () {
        this.highlight = ['', ''];
    };
    return DropAreaTypeCheckComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], DropAreaTypeCheckComponent.prototype, "droppedItemType", void 0);
DropAreaTypeCheckComponent = __decorate([
    core_1.Component({
        selector: 'app-drop-area-type-check',
        templateUrl: './drop-area-type-check.component.html',
        styleUrls: ['./drop-area-type-check.component.css']
    }),
    __metadata("design:paramtypes", [])
], DropAreaTypeCheckComponent);
exports.DropAreaTypeCheckComponent = DropAreaTypeCheckComponent;
//# sourceMappingURL=drop-area-type-check.component.js.map