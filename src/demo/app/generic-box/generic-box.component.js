"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GenericBoxComponent = (function () {
    function GenericBoxComponent() {
    }
    GenericBoxComponent.prototype.ngOnInit = function () {
        if (!this.genericBox) {
            this.genericBox = { name: 'Generic Box 1', content: 'Generic Box 1 Content' };
        }
    };
    return GenericBoxComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], GenericBoxComponent.prototype, "genericBox", void 0);
GenericBoxComponent = __decorate([
    core_1.Component({
        selector: 'app-generic-box',
        templateUrl: './generic-box.component.html',
        styleUrls: ['./generic-box.component.css']
    }),
    __metadata("design:paramtypes", [])
], GenericBoxComponent);
exports.GenericBoxComponent = GenericBoxComponent;
//# sourceMappingURL=generic-box.component.js.map