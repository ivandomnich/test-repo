import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderPipe } from './angular4-order-pipe.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
  	OrderPipe
  ],
  providers: [
    OrderPipe
  ],
  exports: [
  	OrderPipe
  ]
})
export class OrderModule { }





